---
date: 2020-11-17
title: 个人Mac配置
tags:
  - Mac
  - 有用
describe: 个人Mac电脑配置
---

# 开始 {#getting-started}

本章节将帮助你从零开始构建一个基础的 VitePress 文档站点、如果你想改造已有的项目，并希望保留原文档，请从第 3 步开始操作。

- **步骤 1：** 创建一个新目录，并进入该目录

  ```bash
  $ mkdir vitepress-starter && cd vitepress-starter
  ```

- **步骤 2：** 使用你常用的包管理工具初始化 package.json

  ```bash
  $ yarn init
  ```

- **步骤 3：** 本地安装 VitePress

  ```bash
  $ yarn add --dev vitepress
  ```